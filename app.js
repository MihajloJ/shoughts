var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
require('./config/database');

var routes = require('./app_api/routes/index');
var users = require('./app_api/routes/users');
var login = require('./app_api/routes/login');
// var admin = require('./app_api/routes/admin');

var app = express();
/*
var zerorpc = require("zerorpc");

var client = new zerorpc.Client();
client.connect("tcp://127.0.0.1:4242");

client.invoke("Welcome to Shoughts", function(error, res, more) {
	    console.log(res);
});
*/
////////////////
app.set('view engine', 'jade');
app.set('views', __dirname + '/public/views');
// view engine setup
// izbrisao sam view engine jer je on za picke - leonardo da vinci
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api', users);
app.use('/', routes);
app.use('/', login);
app.use(favicon(__dirname + '/public/favicon.ico'));
// app.use('/admin', admin);


/*  namestam route
app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
})
*/


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  res.render('error404');
});

// error handlers

// development error handler
// will print stacktrace
// BITNO:Kada launchamo app, prebacujemo node_env varijablu u production, kako ne bi usporavali server debug shitom-leonardo da michaelangelo
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error404', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error404', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
console.log("********************** SHOUGHTS ONLINE ******************************")
