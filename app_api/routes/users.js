var express = require('express');
var router = express.Router();
var ctrlUsers = require('../controllers/users');
var ctrlPosts = require('../controllers/posts');
var ctrlMails = require('../controllers/mails');

router.get('/users', ctrlUsers.usersReadAll);
router.get('/users/:userid', ctrlUsers.usersReadOne);
router.post('/users', ctrlUsers.usersCreateOne);
router.delete('/users/:userid', ctrlUsers.usersDeleteOne);
router.put('/api/users/:userid', ctrlUsers.usersUpdateOne);

router.get('/posts/', ctrlPosts.postsReadAll);
router.get('/posts/:userid', ctrlPosts.postsReadAllFromOneUser);
router.get('/posts/f/:userid', ctrlPosts.postsReadAllFromAllFollowers);
router.post('/posts/:userid', ctrlPosts.postsCreateOne);
router.get('/posts/:userid/:postid', ctrlPosts.postsReadOne);
router.delete('/posts/:userid/:postid', ctrlPosts.postsDeleteOne);
// router.put('/posts/:userid/:postid', ctrlPosts.postsUpdateOne); SERVER FAILA PRI STARTU IZ NEKOG RAZLOGA

router.post('/mail', ctrlMails.mailCreateOne);

module.exports = router;
